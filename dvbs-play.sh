#!/usr/bin/env bash

tsp --verbose -P analyze --ts-analysis --interval 3 -I file --infinite TSFILES/Turksat_12209_H_10000_21_Oct_2023_HABERTURK-1.1Gbyte.ts -O dektec --modulation DVB-S2-8PSK --satellite-frequency 12209000000 --lnb 9750,10600,11700 --convolutional-rate 3/4 --symbol-rate 10000000 --roll-off 0.20 --pilots
