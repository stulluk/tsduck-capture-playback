#!/usr/bin/env bash

# This command is used to play DUK Digital 3&4 TS file from UK DVB-T

tsp --verbose -I file ~/ROKU/STREAM/DUK_LCN100_D34_v6.ts --infinite -O dektec --modulation DVB-T --frequency 474000000 --bandwidth 8 --guard-interval 1/32 --transmission-mode 8K --convolutional-rate 3/4
