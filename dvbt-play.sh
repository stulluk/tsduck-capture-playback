#!/usr/bin/env bash

set -euo pipefail

if [ $# -gt "0" ]
then
	TSFILE=$1
else
	if [ -d /media/WINDOWS ]
	then
		TSDIRPATH="/media/WINDOWS/TS-FILES/"
	else
		TSDIRPATH="/media/G550"
	fi
fi

# Ask the user which type of stream they want to play
read -rp "Do you want to play DVB-T or DVB-T2? (Enter 't' for DVB-T, 't2' for DVB-T2): " choice

# Set TSFILE based on user's choice
if [ "$choice" = "t" ]; then
	TSFILE="${TSDIRPATH}/PLAY.PL-TS-STREAMS/DVB-T_522_MUX3_4h-2Gbyte.ts"
	printf "TSFILE is %s\n" "${TSFILE}"
	MODULATION="DVB-T"
	FREQ="522000000"
	BANDWIDTH="8"
	GUARD_INTERVAL="1/8"
	TRANSMISSION_MODE="8K"
	printf "MODULATION = %s\n" "${MODULATION}"
	printf "FREQ = %s\n" "${FREQ}"
	printf "BANDWIDTH = %s\n" "${BANDWIDTH}"
	printf "GUARD_INTERVAL = %s\n" "${GUARD_INTERVAL}"
	printf "TRANSMISSION_MODE = %s\n" "${TRANSMISSION_MODE}"

	# Call tsp with variables
	tsp --verbose -I file "${TSFILE}" --infinite -O dektec \
		--modulation "${MODULATION}" \
		--frequency "${FREQ}" \
		--bandwidth "${BANDWIDTH}" \
		--guard-interval "${GUARD_INTERVAL}" \
		--transmission-mode "${TRANSMISSION_MODE}"

elif [ "$choice" = "t2" ]; then
	TSFILE="${TSDIRPATH}/PLAY.PL-TS-STREAMS/PL_DVB-T2_2023-11-10_MUX2_538MHz.ts"
	printf "TSFILE is %s\n" "${TSFILE}"
	MODULATION="DVB-T2"
	FREQ="538000000"
	BANDWIDTH="8"
	FFT_MODE="32K"
	T2_GUARD_INTERVAL="19/256"
	T2_FPSF="2"
	PILOT_PATTERN="4"
	PLP0_TYPE="1"
	PLP0_MODULATION="256-QAM"
	PLP0_CODE_RATE="3/4"
	PLP0_FEC_TYPE="64K"
	PAPR="NONE"
	MISO="OFF"
	printf "MODULATION = %s\n" "${MODULATION}"
	printf "FREQ = %s\n" "${FREQ}"
	printf "BANDWIDTH = %s\n" "${BANDWIDTH}"
	printf "FFT_MODE = %s\n" "${FFT_MODE}"
	printf "T2_GUARD_INTERVAL = %s\n" "${T2_GUARD_INTERVAL}"
	printf "T2_FPSF = %s\n" "${T2_FPSF}"
	printf "PILOT_PATTERN = %s\n" "${PILOT_PATTERN}"
	printf "PLP0_TYPE = %s\n" "${PLP0_TYPE}"
	printf "PLP0_MODULATION = %s\n" "${PLP0_MODULATION}"
	printf "PLP0_CODE_RATE = %s\n" "${PLP0_CODE_RATE}"
	printf "PLP0_FEC_TYPE = %s\n" "${PLP0_FEC_TYPE}"
	printf "PAPR = %s\n" "${PAPR}"
	printf "MISO = %s\n" "${MISO}"

	# Call tsp with variables
	tsp --verbose -I file "${TSFILE}" --infinite -O dektec \
		--modulation "${MODULATION}" \
		--frequency "${FREQ}" \
		--bandwidth "${BANDWIDTH}" \
		--fft-mode "${FFT_MODE}" \
		--t2-guard-interval "${T2_GUARD_INTERVAL}" \
		--t2-fpsf "${T2_FPSF}" \
		--pilot-pattern "${PILOT_PATTERN}" \
		--plp0-type "${PLP0_TYPE}" \
		--plp0-modulation "${PLP0_MODULATION}" \
		--plp0-code-rate "${PLP0_CODE_RATE}" \
		--plp0-fec-type "${PLP0_FEC_TYPE}" \
		--papr "${PAPR}" \
		--miso "${MISO}"

else
	echo "Invalid choice. Exiting."
	exit 1
fi



