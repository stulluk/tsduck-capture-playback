#!/usr/bin/env bash

set -euo pipefail

# Default values for the script
ADAPTER_NUM=0
FRONTEND_NUM=1
DEMUX_NUM=0
DVR_NUM=0

FREQMIN=10700
FREQMAX=12750

SYMRATEMIN=1000
SYMRATEMAX=50000

# Function to display usage
usage() {
    echo "Usage: $0 [SATELLITE DISEQC DVBTYPE MODULATION FREQ POLARITY SYMRATE]"
    echo "Example: $0 Astra_28 0 s QPSK 11500 V 27500"
    exit 1
}

# Function to validate frequency and symbol rate
validate_number() {
    local num=$1
    local min=$2
    local max=$3
    local type=$4

    if [[ -z "${num}" || "${num}" =~ [^0-9] || "${num}" -lt "${min}" || "${num}" -gt "${max}" ]]; then
        printf "Please enter a valid ${type} between ${min} and ${max}.\n"
        exit 1
    fi
}

# Function to validate satellite name
validate_satellite_name() {
    local name=$1

    if [[ ${#name} -gt 10 || ! ${name} =~ ^[a-zA-Z0-9_]+$ ]]; then
        printf "Please enter a valid satellite name (max 10 characters, alphanumeric and underscores only).\n"
        exit 1
    fi
}

# Function to validate DISEQC port number
validate_diseqc() {
    local port=$1

    if [[ -z "${port}" || "${port}" =~ [^0-9] || "${port}" -lt 0 || "${port}" -gt 3 ]]; then
        printf "Please enter a valid DISEQC port number between 0 and 3.\n"
        exit 1
    fi
}

# Check if user provided arguments
if [ "$#" -eq 7 ]; then
    SAT=$1
    DISEQC=$2
    DVBTYPE=$3
    MOD=$4
    FREQ=$5
    POL=$6
    SYMRATE=$7

    validate_satellite_name "${SAT}"
    validate_diseqc "${DISEQC}"

    case ${DVBTYPE} in
        s|S) DVBTYPE="DVB-S"
            MODULATION="QPSK"
            ;;
        s2|S2) DVBTYPE="DVB-S2"
            case ${MOD} in
                qpsk|QPSK) MODULATION="QPSK" ;;
                8psk|8PSK) MODULATION="8-PSK" ;;
                *) printf "Enter QPSK or 8PSK..\n"
                    exit 1 ;;
            esac
            ;;
        *) printf "Enter s or s2..\n"
            exit 1 ;;
    esac

    case ${POL} in
        v|V) POL="vertical"
            POL_TSP="v"
            ;;
        h|H) POL="horizontal"
            POL_TSP="h"
            ;;
        *) printf "Enter v or h..\n"
            exit 1 ;;
    esac

    validate_number "${FREQ}" ${FREQMIN} ${FREQMAX} "frequency"
    validate_number "${SYMRATE}" ${SYMRATEMIN} ${SYMRATEMAX} "symbol rate"

else
    if [ "$#" -ne 0 ]; then
        usage
    fi

    read -p "Enter Satellite Name (max 10 characters, alphanumeric and underscores only): " SAT
    validate_satellite_name "${SAT}"

    read -p "Enter DISEQC port number (0-3): " DISEQC
    validate_diseqc "${DISEQC}"

    read -p "Enter DVB DELIVERY_SYSTEM TYPE: S or S2: " DVBTYPE

    case ${DVBTYPE} in
        s|S) DVBTYPE="DVB-S"
            MODULATION="QPSK"
            ;;
        s2|S2) DVBTYPE="DVB-S2"
            read -p "Select Modulation: QPSK or 8PSK: " MOD
            case ${MOD} in
                qpsk|QPSK) MODULATION="QPSK"
                    ;;
                8psk|8PSK) MODULATION="8-PSK"
                    ;;
                *) printf "Enter QPSK or 8PSK..\n"
                    exit 1;;
            esac
            ;;
        *) printf "Enter s or s2..\n"
            exit 1;;
    esac

    printf "DVBTYPE is ${DVBTYPE}, Modulation is ${MODULATION}\n"

    read -p "Enter Frequency: " FREQ
    validate_number "${FREQ}" ${FREQMIN} ${FREQMAX} "frequency"

    printf "FREQUENCY is ${FREQ}\n"

    read -p "Enter Polarity: V or H: " POL

    case ${POL} in
        v|V) POL="vertical"
            POL_TSP="v"
            ;;
        h|H) POL="horizontal"
            POL_TSP="h"
            ;;
        *) printf "Enter v or h..\n"
            exit 1;;
    esac

    printf "POLARITY is ${POL}\n"

    read -p "Enter Symbol Rate: " SYMRATE
    validate_number "${SYMRATE}" ${SYMRATEMIN} ${SYMRATEMAX} "symbol rate"

    printf "SYMRATE is ${SYMRATE}\n"
fi

CURDATE="$(date +'%Y-%m-%d-%H-%M-%S')"
OUT_TS_FILE="${SAT}_${FREQ}_${POL_TSP}_${SYMRATE}_${CURDATE}.ts"

printf "Output TS file is %s\n" ${OUT_TS_FILE}

tsp --debug -I dvb \
    -d /dev/dvb/adapter${ADAPTER_NUM}:${FRONTEND_NUM}:${DEMUX_NUM}:${DVR_NUM} \
    --delivery-system ${DVBTYPE} \
    --modulation ${MODULATION} \
    --fec auto \
    --satellite-number ${DISEQC} \
    --lnb Extended \
    --frequency "${FREQ}000000" \
    --polarity ${POL} \
    --symbol-rate "${SYMRATE}000" -P until --seconds 60 -P analyze --interval 2 --ts-analysis --cumulative -O file ${OUT_TS_FILE}

