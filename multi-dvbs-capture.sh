#!/bin/bash

# Function to display usage
usage() {
    echo "Usage: $0 <url> <satellite_name> <diseqc_port>"
    echo "Example: $0 \"http://example.com/sat.ini\" Astra_28 0"
    exit 1
}

# Check if the correct number of arguments is passed
if [ "$#" -ne 3 ]; then
    usage
fi

URL=$1
SATELLITE_NAME=$2
DISEQC_PORT=$3
INI_FILE="${SATELLITE_NAME}.ini"

# Validate satellite name
validate_satellite_name() {
    local name=$1

    if [[ ${#name} -gt 10 || ! ${name} =~ ^[a-zA-Z0-9_]+$ ]]; then
        echo "Please enter a valid satellite name (max 10 characters, alphanumeric and underscores only)."
        exit 1
    fi
}

# Validate DISEQC port number
validate_diseqc() {
    local port=$1

    if [[ -z "${port}" || "${port}" =~ [^0-9] || "${port}" -lt 0 || "${port}" -gt 3 ]]; then
        echo "Please enter a valid DISEQC port number between 0 and 3."
        exit 1
    fi
}

validate_satellite_name "${SATELLITE_NAME}"
validate_diseqc "${DISEQC_PORT}"

# Download the ini file
wget -O "$INI_FILE" "$URL"

# Check if the file was downloaded successfully
if [ ! -f "$INI_FILE" ]; then
    echo "Failed to download file from $URL"
    exit 1
fi

# Parse the ini file and run satcapture.sh for each transponder
while read -r line; do
    # Skip comments and empty lines
    if [[ $line =~ ^\; ]] || [[ -z $line ]]; then
        continue
    fi

    # Extract the relevant part of the line
    if [[ $line =~ ^[0-9]+= ]]; then
        IFS=',' read -r -a params <<< "${line#*=}"
        FREQ=$(echo "${params[0]}" | tr -d '\r')
        POLARITY=$(echo "${params[1]}" | tr -d '\r')
        SR=$(echo "${params[2]}" | tr -d '\r')
        FEC=$(echo "${params[3]}" | tr -d '\r')
        SYSTEM=$(echo "${params[4]}" | tr -d '\r')
        MOD=$(echo "${params[5]}" | tr -d '\r')

        # Ignore if the symbol rate is less than 3000
        if (( SR < 3000 )); then
            echo "Ignoring transponder with symbol rate ${SR} (less than 3000)"
            continue
        fi

        # Determine DVB type based on SYSTEM value
        case ${SYSTEM} in
            DVB-S) DVBTYPE="s";;
            S2|DVB-S2) DVBTYPE="s2";;
            *) echo "Unsupported DVB type: ${SYSTEM}"
               exit 1;;
        esac

        # Call satcapture.sh with the extracted parameters
        satcapture.sh "$SATELLITE_NAME" "$DISEQC_PORT" "$DVBTYPE" "$MOD" "$FREQ" "$POLARITY" "$SR"
    fi
done < "$INI_FILE"

# Clean up
rm "$INI_FILE"

