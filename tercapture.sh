#!/usr/bin/env bash

# Safety settings to exit on errors and unset variables
set -euo pipefail

# Constants
ADAPTER_NUM=0
FRONTEND_NUM=0
DEMUX_NUM=0
DVR_NUM=0

FREQMIN=50  # Minimum frequency in MHz
FREQMAX=990  # Maximum frequency in MHz

# Function to get valid input from a predefined list
get_valid_input() {
  local prompt="$1"
  shift
  local allowed_values=("$@")
  local input

  while true; do
    read -p "$prompt (${allowed_values[*]}): " input
    # Check if the input matches any allowed value exactly
    if [[ " ${allowed_values[*]} " =~ " $input " ]]; then
      echo "$input"
      return
    else
      echo "Invalid input. Please enter one of the following: ${allowed_values[*]}."
    fi
  done
}

# Function to get a valid number within a given range
get_valid_number() {
  local prompt="$1"
  local min_val="$2"
  local max_val="$3"
  local num
  
  while true; do
    read -p "$prompt" num
    if [[ $num =~ ^[0-9]+$ ]] && (( num >= min_val && num <= max_val )); then
      echo "$num"
      return
    else
      echo "Invalid input. Please enter a number between $min_val and $max_val."
    fi
  done
}

# Get the frequency in MHz and convert to Hz
FREQ=$(get_valid_number "Enter Frequency in MHz: " $FREQMIN $FREQMAX)
FREQ_HZ=$(($FREQ * 1000000))  # Convert to Hz

# Get DVB standard and convert to full form
dvb_standard=$(get_valid_input "Select the DVB standard (dvbt or dvbt2)" dvbt dvbt2)

# Convert to full form for tsp
if [ "$dvb_standard" = "dvbt" ]; then
    dvb_standard="DVB-T"
elif [ "$dvb_standard" = "dvbt2" ]; then
    dvb_standard="DVB-T2"
else
    echo "Invalid DVB standard."
    exit 1
fi

# Initialize other parameters based on DVB standard
if [ "$dvb_standard" = "DVB-T" ]; then
    echo "You selected DVB-T."

    # Get valid modulation
    modulation=$(get_valid_input "Select modulation scheme" QPSK 16-QAM 64-QAM)

    # Get valid FFT size
    fft_size=$(get_valid_input "Select FFT size" 2K 8K)

    # Get valid guard interval
    guard_interval=$(get_valid_input "Select guard interval" 1/4 1/8 1/16 1/32)

    # Get valid FEC
    fec=$(get_valid_input "Select FEC" 1/2 2/3 3/4 5/6 7/8)


    # Get valid bandwidth
    bandwidth=$(get_valid_input "Select bandwidth" 6 7 8)

elif [ "$dvb_standard" = "DVB-T2" ]; then
    echo "You selected DVB-T2."

    # Get valid modulation
    modulation=$(get_valid_input "Select modulation scheme" QPSK 16-QAM 64-QAM 256-QAM)

    # Get valid FFT size
    fft_size=$(get_valid_input "Select FFT size" 1K 2K 4K 8K 16K 32K)

    # Get valid FEC
    fec=$(get_valid_input "Select FEC" 1/2 3/5 2/3 3/4 4/5 5/6 1/3 2/5 3/5 8/9 9/10)

    # Get valid guard interval
    guard_interval=$(get_valid_input "Select guard interval" 1/4 1/8 1/16 1/32 1/64 1/128 19/256)

    # Get valid bandwidth
    bandwidth=$(get_valid_input "Select bandwidth" 1.7 5 6 7 8 10)

else
    echo "Invalid DVB standard."
    exit 1
fi

# Output selected parameters
echo "Modulation: $modulation, FFT Size: $fft_size, Guard Interval: $guard_interval, Bandwidth: $bandwidth MHz"

# Configuration files and output transport stream
CURDATE="$(date +'%Y-%m-%d-%H-%M-%S')"
OUT_TS_FILE="${dvb_standard}_${FREQ_HZ}_${fft_size}_${bandwidth}MHZ_${fec//\//fec}_${guard_interval//\//gi}_${CURDATE}.ts"

# Construct the tsp command
tsp_command="tsp --debug -I dvb \
  -d /dev/dvb/adapter${ADAPTER_NUM}:${FRONTEND_NUM}:${DEMUX_NUM}:${DVR_NUM} \
  --delivery-system ${dvb_standard} \
  --frequency ${FREQ_HZ} \
  --bandwidth ${bandwidth} \
  --transmission-mode ${fft_size} \
  --low-priority-fec ${fec} \
  --high-priority-fec ${fec} \
  --guard-interval ${guard_interval}"

# Print the tsp command
echo "Executing tsp command:"
echo "$tsp_command"

# Execute the tsp command
$tsp_command > "${OUT_TS_FILE}"

