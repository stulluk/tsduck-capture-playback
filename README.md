# tsduck-capture-playback

These are my custom quick & dirty bash scripts running under linux to capture and playback DVB streams via TSduck.

I use DTU-315 for playback and TBS5530 for Capture

I think the names of the scripts are pretty self explanatory.

I have Turksat antenna at Diseqc port=1 and Hotbird at Diseqc port=2

Merge / Pull Requests are welcome.

